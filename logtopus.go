package logtopus

import (
"fmt"
. "gitlab.com/buddyspencer/chameleon"
)

func Infof(format string, args ...interface{}) {
	fmt.Println(fmt.Sprintf("%s%s",Green("INFO"), "[bla]") + fmt.Sprintf(format, args))
}